package deloitte.academy.lesson01.model;
/**
 * Class of employee atributes
 * @author Franco Duhalt
 * @since 11/03/2020
 * @version 0.1
 */
public class Employee {
	String Attribute01 = "";
	String Attribute02 = "";
	String Attribute03 = "";
	String Attribute04 = "";
	String Attribute05 = "";
	String Attribute06 = "";
	String Attribute07 = "";
	String Attribute08 = "";
	String Attribute09 = "";
	String Attribute10 = "";
	String Attribute11 = "";
	String Attribute12 = "";
	String Attribute13 = "";
	String Attribute14 = "";
	String Attribute15 = "";
	String Attribute16 = "";
	String Attribute17 = "";
	String Attribute18 = "";
	String Attribute19 = "";
	String Attribute20 = "";
	
	public String getAttribute01() {
		return Attribute01;
	}
	public void setAttribute01(String attribute01) {
		Attribute01 = attribute01;
	}
	public String getAttribute02() {
		return Attribute02;
	}
	public void setAttribute02(String attribute02) {
		Attribute02 = attribute02;
	}
	public String getAttribute03() {
		return Attribute03;
	}
	public void setAttribute03(String attribute03) {
		Attribute03 = attribute03;
	}
	public String getAttribute04() {
		return Attribute04;
	}
	public void setAttribute04(String attribute04) {
		Attribute04 = attribute04;
	}
	public String getAttribute05() {
		return Attribute05;
	}
	public void setAttribute05(String attribute05) {
		Attribute05 = attribute05;
	}
	public String getAttribute06() {
		return Attribute06;
	}
	public void setAttribute06(String attribute06) {
		Attribute06 = attribute06;
	}
	public String getAttribute07() {
		return Attribute07;
	}
	public void setAttribute07(String attribute07) {
		Attribute07 = attribute07;
	}
	public String getAttribute08() {
		return Attribute08;
	}
	public void setAttribute08(String attribute08) {
		Attribute08 = attribute08;
	}
	public String getAttribute09() {
		return Attribute09;
	}
	public void setAttribute09(String attribute09) {
		Attribute09 = attribute09;
	}
	public String getAttribute10() {
		return Attribute10;
	}
	public void setAttribute10(String attribute10) {
		Attribute10 = attribute10;
	}
	public String getAttribute11() {
		return Attribute11;
	}
	public void setAttribute11(String attribute11) {
		Attribute11 = attribute11;
	}
	public String getAttribute12() {
		return Attribute12;
	}
	public void setAttribute12(String attribute12) {
		Attribute12 = attribute12;
	}
	public String getAttribute13() {
		return Attribute13;
	}
	public void setAttribute13(String attribute13) {
		Attribute13 = attribute13;
	}
	public String getAttribute14() {
		return Attribute14;
	}
	public void setAttribute14(String attribute14) {
		Attribute14 = attribute14;
	}
	public String getAttribute15() {
		return Attribute15;
	}
	public void setAttribute15(String attribute15) {
		Attribute15 = attribute15;
	}
	public String getAttribute16() {
		return Attribute16;
	}
	public void setAttribute16(String attribute16) {
		Attribute16 = attribute16;
	}
	public String getAttribute17() {
		return Attribute17;
	}
	public void setAttribute17(String attribute17) {
		Attribute17 = attribute17;
	}
	public String getAttribute18() {
		return Attribute18;
	}
	public void setAttribute18(String attribute18) {
		Attribute18 = attribute18;
	}
	public String getAttribute19() {
		return Attribute19;
	}
	public void setAttribute19(String attribute19) {
		Attribute19 = attribute19;
	}
	public String getAttribute20() {
		return Attribute20;
	}
	public void setAttribute20(String attribute20) {
		Attribute20 = attribute20;
	}
	
	
}
