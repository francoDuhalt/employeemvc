package deloitte.academy.lesson01.controller;
/**
 * Clase de condicionales
 * @author Franco Duhalt
 * @since 11/03/2020
 * @version 0.1
 */
public class Condicionales {
	/**
	 * Method for comparing numbers
	 * @param x
	 * @param y
	 * @return string
	 */
	public static String comparar(int x, int y) {
		String mayor=(x>y)?"El n�mero es mayor a." + y: "El n�mero es menor o igual a " + x;
		return mayor;
	}
	
	/**
	 * Method for comparing a boolean
	 * @param flag
	 * @return string
	 */
	public static String bandera(boolean flag) {
		String resultado =(flag)?"El pastel es de verdad.": "El pastel es una mentira.";
		return resultado;
	}
	
	/**
	 * Method to compare a string
	 * @param cliente
	 * @return string
	 */
	public static String descuento(String cliente) {
		String resultado =(cliente == "trabajador")?"Hazle un descuento.": "Cobrale normal.";
		return resultado;
	}
	
	/**
	 * Method to compare with if
	 * @param score
	 * @return string
	 */
	
	public static String contratacion(int score) {
		String resultado = "";
		
		if (score >= 80) {
			resultado = "Estas contratado";
		}else {
			resultado = "Mejor suerte para la proxima";
		}
		
		return resultado;
	}
	
	/**
	 * Method to compare with else if
	 * @param religion
	 * @return string
	 */
	
	public static String religion(String religion){
		String resultado = "";
		
		if ( religion == "Budah") {
			resultado = "Eres budista";
		} else if ( religion == "Judia") {
			resultado = "Eres judio";
		} else {
			resultado = "Eres catolico";
		}
		
		return resultado;
	}
	
}
