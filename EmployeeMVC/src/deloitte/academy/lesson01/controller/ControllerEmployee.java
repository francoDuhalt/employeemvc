package deloitte.academy.lesson01.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import deloitte.academy.lesson01.model.Employee;

/**
 * 
 * @author Franco Duhalt
 * @since 11/03/2020
 * @version 0.1
 */
public class ControllerEmployee {
	private static final Logger LOGGER = Logger.getLogger(ControllerEmployee.class.getName());
	
	/**
	 * Method for adding a object of type employee into an arrayList of type Employee
	 * @param employeeList
	 * @param newEmployee
	 * @throws exception
	 * @return string
	 */
	public static String addEmployee(List<Employee> employeeList, Employee newEmployee) {
		String resultado = "";
		
		try {
			employeeList.add(newEmployee);
			resultado = "Se ha agregado un nuevo empleado.";
		}catch(Exception e){
			resultado = "No se ha podido agregar el registro.";
		}
		
		return  resultado;
	}
	
	/**
	 * Method to add an attribute to an arrayList
	 * @param employeeList
	 * @param newAttribute
	 * @param id
	 * @return  ArrayList<String>
	 */
	public static ArrayList<String> addAttribute(List<Employee> employeeList, String newAttribute, String id) {
		ArrayList<String> resultado = new ArrayList<String>();
		
		try {
			employeeList.forEach(n -> {
				if(n.getAttribute01() == id) {
					n.setAttribute02(newAttribute);
					resultado.add(n.getAttribute01() + ", " + n.getAttribute02());
				}
			});
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No se ha podido agregar el atributo", e);
		}
		return resultado;
	}
	
	/**
	 * Method for finding an attribute on a arrayList of type employees
	 * @param employeeList
	 * @param searchAttribute
	 * @return ArrayList<String>
	 */
	public static ArrayList<String> searchByAttribute(List<Employee> employeeList, String searchAttribute) {
		ArrayList<String> resultado = new ArrayList<String>();
		
		try {
			employeeList.forEach(n ->{
				if (n.getAttribute01() == searchAttribute) {
					resultado.add(n.getAttribute01() + ", " + n.getAttribute02());
				}
			});
		}catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No se ha podido hacer la busqueda por atributo", e);
		}
		
		return resultado;
	}
	
	/**
	 * Method for searchAll in an arrayList
	 * @param employeeList
	 * @return ArrayList<String>
	 */
	public static ArrayList<String> searchAll(List<Employee> employeeList) {
		ArrayList<String> resultado = new ArrayList<String>();
		
		try {
			employeeList.forEach(n -> resultado.add(n.getAttribute01() + ", " + n.getAttribute02()));
		}catch (Exception e) {
			LOGGER.log(Level.SEVERE, "No se ha podido hacer la busqueda", e);
		}
		
		return resultado;
	}

}
