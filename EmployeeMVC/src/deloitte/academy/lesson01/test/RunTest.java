package deloitte.academy.lesson01.test;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson01.controller.Condicionales;
import deloitte.academy.lesson01.controller.ControllerEmployee;
import deloitte.academy.lesson01.model.Employee;

public class RunTest {
	
	public static final List<Employee> employeeList = new ArrayList<Employee>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ControllerEmployee actions = new ControllerEmployee();
		Condicionales actions2 = new Condicionales();
		Employee employee1 = new Employee();
		Employee employee2 = new Employee();
		employee1.setAttribute01("1");
		employee2.setAttribute01("2");
		employee2.setAttribute02("Jojo Rabbit");
		
		System.out.println(actions.addEmployee(employeeList, employee1));
		System.out.println(actions.addEmployee(employeeList, employee2));
		System.out.println(actions.addAttribute(employeeList, "Avengers", "1"));
		System.out.println(actions.searchByAttribute(employeeList, "1"));
		System.out.println(actions.searchAll(employeeList));
		System.out.println(actions2.bandera(true));
		System.out.println(actions2.comparar(10, 11));
		System.out.println(actions2.contratacion(70));
		System.out.println(actions2.descuento("trabajador"));
		System.out.println(actions2.religion("catolico"));
		
	}

}
